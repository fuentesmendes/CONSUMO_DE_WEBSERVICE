﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FORMULARIO.ServicioNuevoAlumno;

namespace FORMULARIO
{
    public partial class Form1 : Form
    {
        ContenidosSoapClient cont = new ContenidosSoapClient();
        public Form1()
        {
            InitializeComponent();
        }

        

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text.Trim(), nombres = txtNombres.Text.Trim(), apellidos = txtApellidos.Text.Trim(), correo = txtEmail.Text.Trim();
            int edad = Convert.ToInt32(txtEdad.Text.Trim()), sexo = Convert.ToInt32(txtSexo.Text.Trim()), profesion = Convert.ToInt32(txtProfesion.Text.Trim()), pais = Convert.ToInt32(txtPais.Text.Trim());
            string mensaje = cont.NuevoAlumno(codigo, nombres, apellidos, edad, correo, sexo, profesion, pais);
            MessageBox.Show(mensaje);
        }
    }
}
